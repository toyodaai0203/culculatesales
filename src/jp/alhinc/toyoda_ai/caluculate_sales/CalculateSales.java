package jp.alhinc.toyoda_ai.caluculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {
	public static void main(String[] args) {
		//System.out.println("ここにあるファイルを開きます => " + args[0]);
		HashMap<String, String> map = new HashMap<String, String>();
		HashMap<String, Long> salesMap = new HashMap<String, Long>();
		
		BufferedReader br = null;
		
		try {
			File file = new File(args[0],"branch.lst");
			
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null){
				String[] arr = line.split(",",0);

				if(!arr[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				
				if(arr.length >= 3) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
					          
				map.put(arr[0], arr[1]);
				salesMap.put(arr[0],(long)0);
				
			}
	
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
			
		} finally {
			if(br != null) {
				try {
					br.close();
					} catch(IOException e) {
						//System.out.println(e);
					}
			}
		}
		File file2= new File(args[0]);
		File salesfiles[] = file2.listFiles();
			for(int i = 0; i < salesfiles.length; i++) {
			if(salesfiles[i].getName().matches("^[0-9]{8}.rcd$")) {
				//System.out.println(salesfiles[i].getName());
		
				try {		
					FileReader fr = new FileReader(salesfiles[i]);
					br = new BufferedReader(fr);
					String line;
					List<String> list = new ArrayList<String>();
					while((line = br.readLine()) != null){
						list.add(line);
					}
					
					if(!map.containsKey(list.get(0))) {
						System.out.println(salesfiles[i].getName() + "の支店コードが不正です");
						return;
					}
					
				
					long added = salesMap.get(list.get(0));
					added += Long.parseLong(list.get(1));
					salesMap.put(list.get(0),added);
					
					String sum = Long.toString(added);
					if (10 < sum.length()){
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					
					if(list.size() >= 3) {
						System.out.println(salesfiles[i].getName() +"のフォーマットが不正です");
					}
					
				} catch(IOException e) {;
					System.out.println("予期せぬエラーが発生しました");
					return;
				
				} finally {
					if(br != null) {
						try {
							br.close();
							} catch(IOException e) {
								//System.out.println(e);
							}	
					}
				}
			}
		}
		BufferedWriter bw = null;
		try{
			File file_3 = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(file_3);
			bw = new BufferedWriter(fw);
			
			for(String key : salesMap.keySet()) {
			
				bw.write(key + ",");
				bw.write(map.get(key) + ",");
				String line = Long.toString(salesMap.get(key));
				bw.write(line);
				bw.newLine();
			}
			bw.close();
			
		} catch (IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;
			
		} finally {
			if(bw != null) {
				try{
					 bw.close();
				} catch (IOException e){
					//System.out.println(e);
				}
			}

		//System.out.println(map.entrySet());
		//System.out.println(map.values());
		//System.out.println(map.keySet());
		//System.out.println(salesMap.entrySet());
		//System.out.println(salesMap.values());
		//System.out.println(salesMap.keySet());

	

		}
	}		
}
	
		
